import subprocess
from pynput import mouse, keyboard
import pyautogui
from threading import Timer

# 设置定时器时间间隔为0.5秒
interval = 0.2

# 定义一个变量来存储定时器对象
timer = None
working = False

from_click = False

def on_press(key):
    global working
    if key == keyboard.Key.esc:  
        working = not working
        play_sound("/usr/share/sounds/freedesktop/stereo/window-attention.oga")
        print("+++++开启+++++" if working else "-----关闭-----" )

def on_move(x, y):
    global timer, working

    # 如果是程序引起的移动，或者未开启程序，则不处理
    if not working or from_click:
        return
    # 如果之前有定时器，取消
    if timer and timer.is_alive():
        timer.cancel()

    timer = Timer(interval, do_click)
    timer.start()

def do_click():
    global from_click
    from_click = True
    pyautogui.click()
    play_sound("/usr/share/sounds/freedesktop/stereo/audio-volume-change.oga")
    from_click = False

def play_sound(path):
    subprocess.call(["sox", path,"-d"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)  # 播放Linux系统默认提示声音    


# 创建键盘监听器并注册回调函数
keyboard_listener = keyboard.Listener(on_press=on_press)
keyboard_listener.start()

# 创建鼠标监听器并注册回调函数
mouse_listener = mouse.Listener(on_move=on_move)

# 启动鼠标监听器
mouse_listener.start()

print("按 esc 开启/关闭" )
# 让程序保持运行状态
mouse_listener.join()
